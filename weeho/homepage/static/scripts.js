$('document').ready(function(){

  $(".menu-icon").click(function() {  
    $( "#menu" ).toggle();  
  });

  $( "#tabs-navbar" ).tabs();

  $.getJSON('/faq/', function(data) {
    var listQuestions = '';
    $.each(data.questions, function(key, value) {
      listQuestions += '<li class="pt-1 pb-1 border-bottom"><a href="' + value.url + '">' + value.title + '</a></li>';
    });
    $('#faq-list').html(listQuestions);
  });

});