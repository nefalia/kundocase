function spamCheck(name, email, subject, text){
    var isSpam = longTextsAreSpam(text);

    if (!isSpam) {
      isSpam = longSubjectAreSpam(subject);
    }
    if (!isSpam) {
      isSpam = blockedDomainsAreSpam(email);
    }
    if (!isSpam) {
      isSpam = someUserNamesAreSpam(name);
    }
    if (!isSpam) {
      isSpam = wordsInTextAreSpam(text);
    }
   
    return isSpam;
}


function longTextsAreSpam(text){
  if(text.length > 200){
      return true;
  }
  return false;
}


function longSubjectAreSpam(subject){
  if(subject.length > 200){
      return true;
  }
  return false;
}


function blockedDomainsAreSpam(email){
    var isBadDomain = false;    

    if(email.indexOf("spam.com") > -1){
      isBadDomain = true;
    }
    if(email.indexOf("universitydiploma.com") > -1){
      isBadDomain = true;
    }

    return isBadDomain;
}


function someUserNamesAreSpam(name){
    var isBadName = false;    

    if(name.indexOf("thord") > -1){
      isBadName = true;
    }
    if(name.indexOf("curt") > -1){
      isBadName = true;
    }
    if(name.indexOf("madicken") > -1){
      isBadName = true;
    }

    return isBadName;
}


function wordsInTextAreSpam(text){
    var isBadWord = false;

    if(text.indexOf("viagra") > -1){
      isBadName = true;
    }
    if(text.indexOf("buy now") > -1){
      isBadName = true;
    }

    return isBadWord;
}
