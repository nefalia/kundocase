from django.conf.urls import url
from views import startpage, faq

urlpatterns = [
    url(r"^faq/", faq, name="faq"),
    url(r"^$", startpage, name="startpage"),
]
