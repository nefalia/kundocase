from django.http import JsonResponse
from django.shortcuts import render

class Customer:
    def __init__(self, name, url):
        self.name = name
        self.url = url

def startpage(request):
    customers = [
        Customer("Acme Inc", "www.acmeinc.com"),
        Customer("Common Laboratories", "www.commonlabs.com"),
        Customer("Invention Cooperations", "www.inventioncorp.com"),
        Customer("Real Estate and Bananas", "www.realestatebananas.com"),
        Customer("Gov Structures", "www.govstructures.com"),
    ]
    return render(request, "startpage.html", {
        "customers": customers,
    })

def faq(request):
    data = {'questions': [
        {'user': 'Steven Smith', 'title': 'Lorem ipsum dolor sit amet?', 'url': '/faq/1'},
        {'user': 'Jane Lewis', 'title': 'Phasellus interdum nulla in ipsum?', 'url': '/faq/2'},
        {'user': 'John Doe', 'title': 'Pellentesque hendrerit sed tortor non bibendum.', 'url': '/faq/3'},
        {'user': 'Nancy Jackson', 'title': 'Curabitur tempor ornare nulla?', 'url': '/faq/4'},
        {'user': 'Susan Walker', 'title': 'Quisque blandit ipsum ut odio scelerisque blandit.', 'url': '/faq5'},
    ]}
    return JsonResponse(data)
