Instruktioner för att köra igång detta projekt
----------------------------------------------

1. Se till att du har python och pip installerat, projektet
är testat med Python 2.7.

2. Installera Django. Detta görs lättast med `pip install Django==1.9.7`. 

3. Installera testdatabasen (SQLite används) genom att stå i projektroten och köra `python manage.py migrate`.

4. Starta testservern med `python manage.py runserver`.
Dubbelkolla att du inte får några felmeddelanden.

6. Surfa till http://localhost:8000 i din webbläsare där
testsajten nu körs. Kolla att du får en sida med allt innehåll.

Du är nu redo med all setup!
